import { createApp } from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import { createI18n } from 'vue-i18n'


// The structure of the locale message is the hierarchical object structure with each locale as the top property
const messages = {
  en: {
    recipe: {
      coffee: 'Coffee', 
      water: 'Water',
      sweet: 'Sweet',
      acidic: 'Acidic',
      weaker: 'Weaker',
      stronger: 'Stronger',
      finish: 'Finish',
      foursixmethod: 'The 4:6 Method',
      ratio: 'Ratio',
      parameters: 'Parameters',
      steps: 'Steps',
    }
  },
  de: {
    recipe: {
      coffee: 'Kaffee', 
      water: 'Wasser',
      sweet: 'Süß',
      acidic: 'Sauer',
      weaker: 'Schwächer',
      stronger: 'Stärker',
      finish: 'Fertig',
      foursixmethod: 'Die 4:6-Methode',
      ratio: 'Verhältnis',
      parameters: 'Parameter',
      steps: 'Schritte',
    }
  }
}


// 2. Create i18n instance with options
const i18n = createI18n({
locale: 'en', // set locale
fallbackLocale: 'en', // set fallback locale
messages, // set locale messages
})
createApp(App).use(router).use(store).use(i18n).mount('#app')
