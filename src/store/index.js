import { createStore } from 'vuex'

export default createStore({
  state: {
    gramsWater: 300,
    gramsCoffee: 20,
    sweetnessAcidityRatio: .5,
    strength: 2,
    ratio: 15
  },
  mutations: {
    setGramsWater(state, grams) {
      state.gramsWater = grams;
    },
    setGramsCoffee(state, grams) {
      state.gramsCoffee = grams;
    },
    setSweetnessAcidityRatio(state, ratio) {
      state.sweetnessAcidityRatio = ratio;
    },
    setStrength(state, strength) {
      state.strength = strength;
    },
    setRatio(state, ratio) {
      state.ratio = ratio;
    }
  },
  actions: {
    setGramsWater(context, grams) {
      if (grams > 0) {
        context.commit('setGramsWater', grams);
      }
    },
    setGramsCoffee(context, grams) {
      if (grams > 0) {
        context.commit('setGramsCoffee', grams);
      }
    },
    setSweetnessAcidityRatio(context, ratio) {
      if (0 <= ratio && ratio <= 1) {
        context.commit('setSweetnessAcidityRatio', ratio);
      }
    },
    setStrength(context, strength) {
      if (1 <= strength && strength <= 4) {
        context.commit('setStrength', strength);
      }
    },
    setRatio(context, ratio) {
      if (14 <= ratio && ratio <= 20) {
        context.commit('setRatio', ratio);
      }
    }
  },
  modules: {
  }
})
